#include <cxxtest/TestSuite.h>
#include <cmath>
#include <tower.h>
class TowerTest : public CxxTest::TestSuite
{

public:

    void testFloor(void){
        Tower *t = new Tower(1);
        TS_ASSERT_EQUALS(t->Floor(), 1);
        // TS_TRACE("t->I");
        // TS_TRACE(t->I);

        //TS_TRACE("t->S");
        //TS_TRACE(t->S);
        t->Calc(2);                   
        TS_ASSERT_EQUALS(t->Floor(), 2);
        t->Calc(3);                   
        TS_ASSERT_EQUALS(t->Floor(), 2);
        t->Calc(4);                   
        TS_ASSERT_EQUALS(t->Floor(), 3);
        
        t->Calc(5);                   
        TS_ASSERT_EQUALS(t->Floor(), 3);
        
        t->Calc(6);                   
        TS_ASSERT_EQUALS(t->Floor(), 4);
         t->Calc(7);                   
        TS_ASSERT_EQUALS(t->Floor(), 4);
         
        t->Calc(9);                   
        TS_ASSERT_EQUALS(t->Floor(), 5);

        t->Calc(14);                   
        TS_ASSERT_EQUALS(t->Floor(), 6);
       
       
    }

	void testCalculate(void){


        Tower *tower; 
      /*
        # Выходные данные: Два целых числа — номер этажа и порядковый номер слева на этаже.
        #
        # Пример:
        # Вход: 13
        # Выход: 6 2
       
       */
      tower = new Tower(13);
      //TS_ASSERT_EQUALS(tower->S, 6);        
      TS_ASSERT_EQUALS(tower->Floor(), 6);       
      TS_ASSERT_EQUALS(tower->Room(), 2);
/*
 # Вход: 11
        # Выход: 5 3
       */
      tower = new Tower(11);
      TS_ASSERT_EQUALS(tower->Floor(), 5);       
      TS_ASSERT_EQUALS(tower->Room(), 3);
  }
};
