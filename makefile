C++ = g++
CXX = g++
INCLUDE = ~/cxxtest/
TESTGEN = ~/cxxtest/bin/cxxtestgen
SRC = ./src/
all: run_tests tower apple pool

apple:	  
	$(CXX) `pkg-config --cflags --libs opencv` -I $(SRC) $(SRC)apple.cpp -o apple
pool:
	$(CXX) `pkg-config --cflags --libs opencv` -std=c++0x -I $(SRC) $(SRC)pool.cpp -o pool
tower:
	$(CXX) -I $(SRC) $(SRC)tower.cpp -o tower

# A rule that runs the unit tests
run_tests: runner
	./runner -v

# How to build the test runner
runner: runner.cpp
	$(CXX) -I $(INCLUDE) -I $(SRC) -o $@ $<

# How to generate the test runner
runner.cpp: test/towerTest.hpp
	$(TESTGEN) -o $@ --error-printer $^


clean:
	rm -f  runner runner.cpp tower apple pool