#include <iostream>
#include <cstdio>
#include <opencv2/opencv.hpp>
using namespace cv;
const char* path = "img/pool02.jpg";

/*
ЯП: приоритетно С++, можно Python
Дополнительно библиотека OpenCV

Поиск объектов по цвету. Реализовать алгоритм поиска объекта по цветовому патерну. 

В вашу программу будет отправлено фото стола для бильярда, 
на котором будет много шаров разного цвета, ваша задача найти и выделить белый шар. 
*/

bool isWhite(Vec3b colour){
	// std::cout << "R: "<< (int) colour[0] << ", ";
	// std::cout << "G: "<< (int) colour[1] << ", ";
	// std::cout << "B: "<< (int) colour[2] << std::endl;		

	if(colour.val[0] > 220 && colour.val[1] > 220 && colour.val[2] > 220) return true;
	return false;
}

void drawCircle(Mat  mat, Point pt, int radius){
	// draw the circle center
	circle( mat, pt, 3, Scalar(0,255,0), -1, 8, 0 );
	// draw the circle outline
	circle( mat, pt, radius, Scalar(0,0,255), 3, 8, 0 );
}

int main(int argc , char** argv)
{   

	Mat image, center_ball;
	namedWindow("pool",1);	
	for(;;)
	{		
		image = imread(argv[1] , CV_LOAD_IMAGE_COLOR);
		
		if( argc != 2 && ! image.data)
        	return -1;		
		Mat planes[3];
		split(image, planes);  // planes[2] is the red channel
		
		center_ball = planes[2]; // red chanel copy. Нам нужна яркость всех кроме зеленого

		GaussianBlur( center_ball, center_ball, Size(9, 9), 2, 2 );
		std::vector<cv::Vec3f> circles;
 		// отсекаем по размеру,
		cv::HoughCircles(center_ball, 
			circles, 
			CV_HOUGH_GRADIENT, 
			1, 
 			center_ball.rows / 12, // расстояние между центрами
 			100, //1 th 
 			10, // 2 param
 			0, // r min 
 			center_ball.rows / 12 // r max
 			);

		if(circles.size() == 0) std::exit(-1);
		Point pt = Point(1,1);
		int rradius = 1;
		bool isDraw = false;

		for(size_t current_circle = 0; current_circle < circles.size(); ++current_circle) {
			cv::Point center(std::round(circles[current_circle][0]), std::round(circles[current_circle][1]));
			int radius = std::round(circles[current_circle][2]); 
			// drawCircle(image, center, radius );
			if( isWhite(image.at<Vec3b>(center.y, center.x)) ){
				if(rradius < radius) {
					rradius = radius;
					pt = center;
					isDraw = true;
				} 				
			}

		}
		if(isDraw){
			drawCircle(image,pt, rradius);			    
		}
		imshow("pool", image);		
	    imshow("gray", center_ball);
		if(waitKey(30) >= 0) break;
	}
	return 0;
}