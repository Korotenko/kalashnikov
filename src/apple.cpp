#include <opencv2/opencv.hpp>
#include <iostream>
#include <cstdlib>
using namespace cv;
using namespace std;
const char* imagePath = "img/apple02.jpg";
const int fragmentSize = 128;
/*
Задача 3: 
ЯП: приоритетно С++, можно Python 
Дополнительно библиотека OpenCV

Поиск обьекта по шаблону (части изображения). 
Ваша программа должна будет найти на фото объект изображенный на другом фото. 
Например вам будет дано Яблоко которое надо найти в натюрморте картины.
*/
int main(int, char**)
{
    

    Mat image, // main image     
    result,
    image_copy,
    fragment; // random fragment
    
    const char* imageTitle = "Source";
    const char* fragmentTitle = "Fragment";
    const char* resultTitle = "Result";

    image = imread(imagePath , CV_LOAD_IMAGE_COLOR);
    
    
    cout<<"Image size: rows (y)" << image.rows << " cols (x)" << image.cols <<std::endl;
    
    // top left point to syart copy random fragment
    srand (time(NULL));
    Point pt = Point(rand() % (image.cols - fragmentSize) + 1, 1);
    srand (time(NULL));
    pt.y = rand() % (image.rows - fragmentSize)  + 1;

    cout << "Fragment start position : x " << pt.x << " y: "<< pt.y <<std::endl;
    
    image.copyTo(fragment);    
    fragment = Mat(image , Rect(pt.x, pt.y, fragmentSize, fragmentSize));

    int result_cols =  image.cols - fragment.cols + 1;
    int result_rows = image.rows - fragment.rows + 1;

    result.create( result_rows, result_cols, CV_32FC1 );

    namedWindow(imageTitle, WINDOW_AUTOSIZE);
    namedWindow(fragmentTitle, WINDOW_AUTOSIZE);
    namedWindow(resultTitle, WINDOW_AUTOSIZE);

    // matchLoc = maxLoc для TM_CCOEFF_NORMED
    // см. документацию
    matchTemplate(image, fragment, result, TM_CCOEFF_NORMED);    
    normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );

    double minVal; double maxVal; Point minLoc; Point maxLoc;
    Point matchLoc;

    minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );
    matchLoc = maxLoc;
    
    image.copyTo(image_copy);

    rectangle( image_copy, matchLoc, Point( matchLoc.x + fragment.cols , matchLoc.y + fragment.rows ), Scalar(0,255,0), 2, 8, 0 );
    rectangle( result, matchLoc, Point( matchLoc.x + fragment.cols , matchLoc.y + fragment.rows ), Scalar::all(0), 2, 8, 0 );



    imshow(imageTitle, image_copy);
    imshow(fragmentTitle, fragment);
    imshow(resultTitle, result);
    waitKey(0);
    
    return 0;    
}