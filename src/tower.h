
#ifndef __tx__Tower_h__
#define __tx__Tower_h__
#include <iostream>
// 
class Tower{
private:
    int _floor;
    int _left;    
    int squareSum(int n){
        return (n * (n + 1) * (n + n + 1)) / 6;
    }
    int sumOneTN(int n){
        return (n * (n + 1))/2;
    }
public:    
    Tower(int room){
        Calc(room);            
    }
    void Calc(int room){            
        _floor = 1, _left = 1;
        if(room < 2) return;

        int i = 1;
        while (room > squareSum(i++));
        if(i > 2)
            i = i - 2;
        int res = squareSum(i);
        _floor = sumOneTN(i) + 1;
        int ini = i + 1;        
        int rs = room - res - 1;        
        if(rs > 0){
            int lines = rs / ini;            
            _floor += lines;
            if(rs % ini){
                _left = rs % ini + 1;
            }
        }
    }
    int Room(){ return _left;}
    int Floor(){return _floor;}
};

#endif // __tx__Tower_h__
